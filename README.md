<samp>

<p align="center">Hi! I write software, most for web and android.</p>

<p align="center"><a href="https://alexrintt.io">alexrintt.io</a> • <a href="mailto:reach@alexrintt.io">reach@alexrintt.io</a> • <a href="https://github.com/alexrintt">github.com/alexrintt</a></p>

</samp>
